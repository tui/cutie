// SPDX-License-Identifier: GPL-2.0-or-later
/*
 */

/*
https://manpages.ubuntu.com/manpages/xenial/man1/inputattach.1.html

drivers/input$ e touchscreen/touchit213.c

*/

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/kthread.h>
#include <linux/freezer.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/nvram.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/sysfs.h>
#include <linux/backlight.h>
#include <linux/bitops.h>
#include <linux/fb.h>
#include <linux/platform_device.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/input.h>
#include <linux/leds.h>
#include <linux/rfkill.h>
#include <linux/dmi.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/acpi.h>
#include <linux/pci.h>
#include <linux/power_supply.h>
#include <linux/platform_profile.h>
#include <linux/uaccess.h>
#include <linux/tty.h>
#include <linux/serio.h>

// See https://github.com/cutiepi-io/cutiepi-middleware

// Thanks to drivers/misc/ivsc/mei_pse.c .
// Thanks to touchit213.c

// sudo killall cutiepi-mcuproxy

// sudo /usr/local/bin/cutiepi-mcuproxy.no
// cd /my/linux-raspi-5.15.0
// time make drivers/platform/cutiepi/main.ko && sudo insmod drivers/platform/cutiepi/main.ko
// sudo dmesg

// sudo inputattach --baud 115200 --noinit -t213 /dev/ttyS0
// sudo rmmod touchit213

// watch cat /sys/class/power_supply/cutie_bat/*

// tail -f /dev/ttyS0  | hexdump -C

struct cutie {
	struct input_dev *dev;
  	struct power_supply *psy;
	struct serio *serio;
	unsigned char csum;
	char phys[32];

  int battery_voltage;
  int battery_charging;
  
};


/****************************************************************************
 * Battery glue
 */


static int battery_get_property(struct power_supply *psy,
				enum power_supply_property psp,
				union power_supply_propval *val)
{
  struct cutie *me = psy->drv_data;
	int ret = 0;

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
	  if (me->battery_charging == -1)
	    return -EINVAL;
	  if (me->battery_charging)	    
	    val->intval = POWER_SUPPLY_STATUS_CHARGING;
	  else
	    val->intval = POWER_SUPPLY_STATUS_DISCHARGING;
	  break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
	  val->intval = me->battery_voltage;
	  break;
	default:
		return -EINVAL;
	}

	return ret;
}

static enum power_supply_property battery_props[] = {
        POWER_SUPPLY_PROP_STATUS,
        POWER_SUPPLY_PROP_VOLTAGE_NOW,
};

static int battery_setup(struct cutie *me)
{
	struct power_supply_desc *psy_desc;
	struct power_supply *psy;


	psy_desc = kzalloc(sizeof(*psy_desc), GFP_KERNEL);
	if (!psy_desc)
		return -ENOMEM;

	psy_desc->name = "cutie_bat";
	psy_desc->type = POWER_SUPPLY_TYPE_BATTERY;
	psy_desc->properties = battery_props;
	psy_desc->num_properties = ARRAY_SIZE(battery_props);
	psy_desc->get_property = battery_get_property;

	psy = power_supply_register(NULL, psy_desc, NULL);
	if (IS_ERR(psy))
		return dev_err_probe(NULL, PTR_ERR(psy),
				     "failed to register battery\n");
	me->psy = psy;
	psy->drv_data = me;
	return 0;
}

static void battery_teardown(struct cutie *me)
{
	power_supply_unregister(me->psy);
}

/****************************************************************************
 * Input glue
 */

static void input_key(struct cutie *me, int code)
{
		input_event(me->dev, EV_MSC, MSC_SCAN, code);
		input_report_key(me->dev, code, 1);
		input_sync(me->dev);

		input_event(me->dev, EV_MSC, MSC_SCAN, code);
		input_report_key(me->dev, code, 0);
		input_sync(me->dev);
}

/****************************************************************************
 * Main driver
 */

int mcu_write(struct cutie *me, unsigned char *buf, int len)
{
  int i, ret = 0;

  print_hex_dump(KERN_ERR, "write: ", 0, 16, 1, buf, len, true);

  for (i=0; i<len; i++) {
    serio_write(me->serio, buf[i]);
  }
  return ret;
}

static int mcu_handle_command(struct cutie *me, int m_cmd, unsigned long long m_data)
{
  switch (m_cmd)
    {
    case 1: // button
      printk("Got button event %d\n", (int) m_data);
      input_key(me, KEY_H + m_data);
#if 0	  
      emit updateEvent(QString("button"), (int) m_data);

      if (m_data == 1)
	{
	  m_payload.resize(7);
	  m_payload[0] = 0x5A;
	  m_payload[1] = 0xA5;
	  m_payload[2] = 1;
	  m_payload[3] = 1;
	  m_payload[4] = 0;
	  m_payload[5] = 0xF1;
	  m_payload[6] = 0;
	  for (int i = 0; i < 6; i++)
	    m_payload[6] = (m_payload[6] + m_payload[i]);
	  m_serialPort.write(m_payload);
	  m_serialPort.flush();
	}

      if (m_data == 3)
	{
	  m_payload.resize(7);
	  m_payload[0] = 0x5A;
	  m_payload[1] = 0xA5;
	  m_payload[2] = 1;
	  m_payload[3] = 1;
	  m_payload[4] = 0;
	  m_payload[5] = 0xFC;
	  m_payload[6] = 0;
	  for (int i = 0; i < 6; i++)
	    m_payload[6] = (m_payload[6] + m_payload[i]);
	  m_serialPort.write(m_payload);
	  m_serialPort.flush();
	}
#endif
      break;

    case 2: // battery
      printk("Got battery event: %d mV\n", (int) m_data);
      me->battery_voltage = m_data;
      //emit updateEvent(QString("battery"), (int) m_data);
      break;

    case 3: // charging
      printk("Got charging event: %d (4 == charging)\n", (int) m_data);
      me->battery_charging = -1;
      if (m_data == 4)
	me->battery_charging = 1;
      if (m_data == 5)
	me->battery_charging = 0;
      
#if 0
      m_payload.resize(7);
      m_payload[0] = 0x5A;
      m_payload[1] = 0xA5;
      m_payload[2] = 1;
      m_payload[3] = 1;
      m_payload[4] = 0;
      if (m_data == 4)
	m_payload[5] = 0xF4;
      else
	m_payload[5] = 0xF5;
      m_payload[6] = 0;
      for (int i = 0; i < 6; i++)
	m_payload[6] = (m_payload[6] + m_payload[i]);
      m_serialPort.write(m_payload);
      m_serialPort.flush();
#endif
      break;
    case 4: // version
      printk("Got version reply, %d\n", (int) m_data);
      break;
    default:
      printk("Got unknown event, %d / %d\n", (int) m_cmd, (int) m_data);
    }
  return 0;
}


static void mcu_handle_byte(struct cutie *me, unsigned char c)
{
  static int pos;
  static int checksum;
  static int m_cmd;
  static int payloadLength;
  static int data_bytes;
  static unsigned long long m_data;

  checksum += c;
    
  switch (pos) {
  case 0:
    if (c == 0x5a) {
      pos++;
      return;
    }
    printk("Expected 0x5a header\n");
    break;

  case 1: 
    if (c == 0xa5) {
      pos++;
      return;
    }
    printk("Expected 0xa5 header\n");
    break;

  case 2: // msg type
    m_cmd = c;
    pos++;
    return;

  case 3: // length of msg payload
    payloadLength = c;
    pos++;
    return;

  case 4: // length of msg payload
    payloadLength += 256 * c;
    m_data = 0;
    data_bytes = 0;
    pos++;
    return;

  case 5:
    if (m_cmd == 4)
      printk("Version: %c\n", c);
    else
      m_data += (c << data_bytes * 8);
    data_bytes++;

    if (data_bytes >= payloadLength)
      {
	// full data is received regardless number of bytes
	pos++;
      }

    return;

  case 6: // checksum
    checksum -= c;
    if (c == (255 & checksum))
      { // correct uart msg received
	//ExecuteCommand();
	printk("Got valid checksum, cmd %d\n", m_cmd);
	mcu_handle_command(me, m_cmd, m_data);
      }
    else
      {
	printk("Checksum wrong!\n");
      }
    break;

  default:
    printk("Totally unexpected state\n");
    break;
  }
  
  pos = 0;
  checksum = 0;
  payloadLength = 0;
  m_data = 0;
}

static int mcu_send_command(struct cutie *me, unsigned char *buf, int len)
{
  int ret, i;

  // header 
  buf[0] = 0x5A;
  buf[1] = 0xA5;

  // CRC
  buf[len-1] = 0; 
  for (i = 0; i < len-1; i++)
    buf[len-1] = (buf[len-1] + buf[i]);

  ret = mcu_write(me, buf, len);
  if (ret == len)
    return 0;
  return ret;
}

static int mcu_simple_command(struct cutie *me, unsigned char b2, unsigned char b5)
{
  unsigned char buf[7];

  // msg type
  buf[2] = b2;

  // payload length 
  buf[3] = 1;
  buf[4] = 0;

  // payload: get version 
  buf[5] = b5;

  return mcu_send_command(me, buf, 7);
}

static int mcu_get_version(struct cutie *me)
{
  return mcu_simple_command(me, 1, 0xf6);
}

static int mcu_power_off(struct cutie *me)
{
  return mcu_simple_command(me, 5, 0xf7);
}

struct task_struct *thread;

#if 0
static int main_thread(void *data)
{
  char buf[102] = "I should not really do this\n";
  int ret;
  int i;
#define FNAME "/dev/ttyS0"
  //#define FNAME "/tmp/delme"

  printk("Main thread started\n");
  file = filp_open(FNAME, O_RDWR | O_LARGEFILE, 0600);
  if (IS_ERR(file)) {
    printk("filp_open(%s) failed\n", FNAME);
    ret = PTR_ERR(file);
    goto err;
  }
#if 1
  {
    struct tty_struct *tty;
    tty = (struct tty_struct *)file->private_data;
    tty_termios_encode_baud_rate(&tty->termios,B115200,B115200);
    ret = 0;
    if (ret) {
      printk("termios bad\n");
      goto err;
    }
  }
  filp_close(file, NULL);
  file = filp_open(FNAME, O_RDWR | O_LARGEFILE, 0600);
  if (IS_ERR(file)) {
    printk("filp_open(%s) failed\n", FNAME);
    ret = PTR_ERR(file);
    goto err;
  }
#endif
  ret = battery_setup();
  if (ret)
    goto err;
  ret = input_setup();
  if (ret)
    goto err;
  mcu_get_version();

  for (i=0; 1; i++) {
    int j;
    if (thread && kthread_should_stop()) {
      break;
    }
    ret = mcu_read(buf, 100);
    if (ret < 0)
      goto err;
    for (j = 0; j < ret; j++)
      mcu_handle_byte(buf[j]);
    msleep(100);
    if (i == 100) {
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
      mcu_power_off();
    }
    switch (i) {
    case 10: input_key(KEY_H); break;
    case 11: input_key(KEY_E); break;
    case 12: input_key(KEY_L); break;
    case 13: input_key(KEY_L); break;
    case 14: input_key(KEY_O); break;
    }
  }
  input_teardown();
  battery_teardown();
  
  filp_close(file, NULL);
  printk("Success\n");
  return 0;

 err:
  printk("Something went wrong\n");
  return ret;
}
#endif

/****************************************************************************
 * Do the thread-based initialization by hand
 */

#if 0
static int __init cutie_init(void)
{
	int ret;
	printk("Hello world\n");

	if (0) {
	  ret = main_thread(NULL);
	  if (!ret)
	    return -ENODEV;
	  return ret;
	}

	printk("Kthread run starting\n");
	//MOD_INC_USE_COUNT;
	thread = kthread_run(main_thread, NULL, "cutiemon");
	printk("Kthread run returned\n");

	return 0;
}

static void __exit cutie_exit(void)
{
  kthread_stop(thread);
  //MOD_DEC_USE_COUNT;
}


//MODULE_DESCRIPTION(TPACPI_DESC);
MODULE_VERSION("0.0");

module_init(cutie_init);
module_exit(cutie_exit);
#endif


static irqreturn_t cutie_interrupt(struct serio *serio,
		unsigned char data, unsigned int flags)
{
	struct cutie *cutie = serio_get_drvdata(serio);
	struct input_dev *dev = cutie->dev;

	mcu_handle_byte(cutie, data);

	return IRQ_HANDLED;
}

/*
 * cutie_disconnect() is the opposite of cutie_connect()
 */

static void cutie_disconnect(struct serio *serio)
{
	struct cutie *cutie = serio_get_drvdata(serio);

	printk("...disconnect\n");

	input_get_device(cutie->dev);
	input_unregister_device(cutie->dev);
	serio_close(serio);
	serio_set_drvdata(serio, NULL);
	input_put_device(cutie->dev);
	battery_teardown(cutie);
	kfree(cutie);
}

/*
 * cutie_connect() is the routine that is called when someone adds a
 * new serio device that supports the Touchright protocol and registers it as
 * an input device.
 */

static int cutie_connect(struct serio *serio, struct serio_driver *drv)
{
	struct cutie *cutie;
	struct input_dev *input_dev;
	int err, i;

	cutie = kzalloc(sizeof(struct cutie), GFP_KERNEL);
	cutie->battery_voltage = -1;
	cutie->battery_charging = -1;
	
	input_dev = input_allocate_device();
	if (!cutie || !input_dev) {
		err = -ENOMEM;
		goto fail1;
	}

	cutie->serio = serio;
	cutie->dev = input_dev;
	snprintf(cutie->phys, sizeof(cutie->phys),
		 "%s/input0", serio->phys);

	input_dev->name = "Cutiepi power button";
	input_dev->phys = cutie->phys;

	input_dev->id.bustype = BUS_RS232;
	input_dev->id.vendor = SERIO_TOUCHIT213;
	input_dev->id.product = 0;
	input_dev->id.version = 0x0100;
	input_dev->dev.parent = &serio->dev;
	input_dev->evbit[0] = BIT_MASK(EV_KEY);
	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	input_set_capability(input_dev, EV_MSC, MSC_SCAN);
	for (i = 0; i < 255; i++) {
	  input_set_capability(input_dev, EV_KEY, i);
        }

	serio_set_drvdata(serio, cutie);

	err = serio_open(serio, drv);
	if (err)
		goto fail2;

	err = input_register_device(cutie->dev);
	if (err)
		goto fail3;

	err = battery_setup(cutie);
	if (err)
	  goto fail4;
	printk("input dev registered\n");
#if 0
	{
	  int i;
	  for (i=0; i<100; i++)
	    mcu_power_off(cutie);
	}
#endif
	{
	  int i;
	  for (i=0; i<30; i++)
	    mcu_get_version(cutie);
	}

	return 0;

 fail4: /* FIXME. Bad! */
 fail3:	serio_close(serio);
 fail2:	serio_set_drvdata(serio, NULL);
 fail1:	input_free_device(input_dev);
	kfree(cutie);
	return err;
}

/*
 * The serio driver structure.
 */

static const struct serio_device_id cutie_serio_ids[] = {
	{
		.type	= SERIO_RS232,
		.proto	= SERIO_TOUCHIT213,
		.id	= SERIO_ANY,
		.extra	= SERIO_ANY,
	},
	{ 0 }
};

MODULE_DEVICE_TABLE(serio, cutie_serio_ids);

static struct serio_driver cutie_drv = {
	.driver		= {
		.name	= "cutie",
	},
	.description	= "cutie",
	.id_table	= cutie_serio_ids,
	.interrupt	= cutie_interrupt,
	.connect	= cutie_connect,
	.disconnect	= cutie_disconnect,
};

module_serio_driver(cutie_drv);

MODULE_LICENSE("GPL");
